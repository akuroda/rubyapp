require 'capybara'
require 'capybara/dsl'
require 'capybara/rspec'
require 'capybara/poltergeist'
require './web.rb'

# use phantomjs
Capybara.current_driver = :poltergeist
Capybara.app = Sinatra::Application
Capybara.always_include_port = true

Dir.glob("./spec/steps/*step.rb") { |f| require f }
