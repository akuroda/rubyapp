# turnip step definition
require 'spec_helper'

step 'visit top page' do
  visit '/'
end

step 'show top page' do
  expect(page).to have_content 'Hello'
end

step 'input name :user' do |user|
  fill_in 'name', :with => user
end

step 'click submit button' do
  click_button 'submit'
end

step 'show hello :user' do |user|
  expect(page).to have_content "Hello #{user}"
end
