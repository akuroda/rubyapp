require './web'
require 'test/unit'
require 'rack/test'

class MyTest < Test::Unit::TestCase
  include Rack::Test::Methods

  # for Rack::Test
  def app
    Sinatra::Application
  end

  def test_root
    get '/hello-world'
    assert last_response.ok?
    assert_equal 'Hello, world', last_response.body
  end
end
