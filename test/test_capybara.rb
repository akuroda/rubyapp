require './web'
require 'capybara'
require 'capybara/dsl'
require 'test/unit'

class CapybaraTest < Test::Unit::TestCase
  include Capybara::DSL

  def setup
    Capybara.app = Sinatra::Application.new
  end

  def test_root
    visit '/'
    assert page.has_content?('Hello')
  end
end
