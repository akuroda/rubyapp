require './web'
require 'minitest/unit'
require 'rack/test'

class MyMiniTest < MiniTest::Unit::TestCase
  include Rack::Test::Methods

  # for Rack::Test
  def app
    Sinatra::Application
  end

  def test_root
    # set :root, Dir.pwd # not to search views for ruby system directory
    get '/'
    assert last_response.ok?
    assert last_response.body.include? 'Hello, Sinatra'
  end

  def test_hello_world
    get '/hello-world'
    assert last_response.ok?
    assert_equal 'Hello, world', last_response.body
  end

  def test_json
    get '/json'
    assert last_response.ok?
    assert_includes last_response.content_type, 'application/json'
    assert_equal 'bar', JSON.parse(last_response.body).fetch('foo')
  end

end
