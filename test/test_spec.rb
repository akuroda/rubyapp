require './web'
require 'rspec'
require 'rack/test'

describe 'helloworld' do
  include Rack::Test::Methods

  # for Rack::Test
  def app
    Sinatra::Application
  end

  it 'returns hello' do
    get '/hello-world'
    last_response.should be_ok
    last_response.body.should eq 'Hello, world'
  end
end
