require 'sinatra'
require 'json'
require "sinatra/reloader" if development?

get '/' do
  erb :index
end

get '/hello' do
  @name = params[:name]
  erb :hello
end

get '/hello-world' do
  "Hello, world"
end

get '/json' do
  content_type :json
  { foo:"bar" }.to_json
end
